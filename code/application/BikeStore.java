//Tannaz Maghsoodi 2234772
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String args[]){

        Bicycle[] bicycles = new Bicycle[4];

        bicycles[0] = new Bicycle("First Manufacturer", 10, 110.5);
        bicycles[1] = new Bicycle("Second Manufacturer", 6, 30.45);
        bicycles[2] = new Bicycle("Third Manufacturer", 4, 45.3);
        bicycles[3] = new Bicycle("Forth Manufacturer", 12, 10.7);

        for (int i = 0; i < bicycles.length; i++) {
            System.out.println("Bicycle " + (i + 1) + ":");
            System.out.println(bicycles[i]);
            System.out.println();
        }

    }
}
